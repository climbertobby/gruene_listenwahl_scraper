# Grüne Listenwahl

Die Wiener Grünen wählen am 15. Februar 2020 ihre Liste für die Gemeinderatswahl
2020. Ein netter Anlass ein bisschen mit Text-Analyse-Tools zu spielen. 

Der Code hier scraped, parsed und analysiert die Texte der Bewerber\_innen auf 
[listenwahl.wien/kandidatinnen](https://listenwahl.wien/kandidatinnen/).

Ich teile den Code hier, schreibe ihn aber für meinen eigenen Spaß, erwartet
keine Dokumentation.

Bitte lasst `01_scrape.R` jeweils nur einmal laufen und benützt dann die
heruntergeladenen Dateien. Wie bei allen Web-Scraping Projekten gilt: **Bitte
keine überflüssigen automatisierten Anfragen auf
den Server!**