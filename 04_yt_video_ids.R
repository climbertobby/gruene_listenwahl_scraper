require(tidyverse)

load("scraped.Rdata")

# stand back i know regular expressions!
alle_texte %>% 
  map("yt_link") %>% 
  keep(function(x){
    !is.na(x)
  }) %>% 
  unlist() %>% 
  map_chr(function(x){
    if( str_detect(x, fixed("https://www.youtube.com/watch?v=")) ) {
      return(str_extract(x, "(?<=(\\&|\\?)v=)(.*?)(?=(\\&|$))"))
    } else if( str_detect(x, fixed("https://youtu.be/")) ){
      return(str_extract(x, "(?<=https://youtu\\.be/)(.*)"))
    } else {
      return(NA_character_)
    }
  })